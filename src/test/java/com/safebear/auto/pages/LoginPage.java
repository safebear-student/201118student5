package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor

public class LoginPage {
    LoginPageLocators locators = new LoginPageLocators();
    @NonNull
    WebDriver driver;

    public String getPageTitle(){

        return driver.getTitle().toString();
    }

    public void login(String uname , String pword){
        driver.findElement(locators.getUsernameLocator()).sendKeys(uname);
        driver.findElement(locators.getPasswordLocator()).sendKeys(pword);
        driver.findElement(locators.getLoginButtonLocator()).click();
    }

    public void clickLoginButton(){
        driver.findElement(locators.getLoginButtonLocator()).click();
    }
    public String getErrorMessage(){
        return driver.findElement(locators.getWrongLoginError()).toString();



    }
}
