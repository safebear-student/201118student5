package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;


@Data
public class LoginPageLocators {

    private By usernameLocator = By.id("username");
    private By passwordLocator = By.id("password");
    private By LoginButtonLocator = By.id("enter");
    //private By LoginButtonLocator = By.xpath(".//button[@id='enter]");
    private By WrongLoginError = By.xpath("//a[contains(text(), \"WARNING: Username or Password is incorrect\")]");


}
