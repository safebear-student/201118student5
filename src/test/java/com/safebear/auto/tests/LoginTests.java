package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTests {

    @Test
    public void loginTest(){

        //open web app here
        driver.get(Utils.getUrl());

        //step 1 assert we are on the login page
        Assert.assertEquals(loginPage.getPageTitle(),"Login Page","None");
        //login
                loginPage.login("tester","letmein");

        //assert we are on the tools page
        Assert.assertEquals(toolsPage.getPageTitle(),"Tools Page","not there");
        //check login succses message displayed

    }
    @Test
    public void loginTestneg(){

        //open web app here
        driver.get(Utils.getUrl());

        //step 1 assert we are on the login page
        Assert.assertEquals(loginPage.getPageTitle(),"Login Page","None");
        //login
        loginPage.login("tester","letme");

        //assert correct error message is displayed
        Assert.assertEquals(loginPage.getErrorMessage(),"WARNING:Username or Password is incorrect","This is not the page we are looking for");
        //check login succses message displayed

    }

}
